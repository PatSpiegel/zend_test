<?php declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180924225538 extends AbstractMigration
{
    /**
     * Возвращает описание этой миграции.
     */
    public function getDescription()
    {
        return 'This is the initial migration which creates log table.';
    }

    /*
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
//        для тестово задания не стал замарачиваться со связями
//        $db = $schema->createTable('users');
//        $db->addColumn('id', 'integer', ['autoincrement' => true]);
//        $db->addColumn('ip', 'text', ['notnull' => true]);
//        $db->addColumn('unique_visit', 'integer', ['notnull' => true]);
//        $db->addIndex(['ip'],'ip_index');
//        $db->setPrimaryKey(['id']);

        //создаём таблицу 'logs'
        $db = $schema->createTable('logs');
        $db->addColumn('id', 'integer', ['autoincrement' => true]);
        $db->addColumn('ip', 'text', ['notnull' => true]);
        $db->addColumn('url_from', 'text', ['notnull' => true]);
        $db->addColumn('url_to', 'text', ['notnull' => true]);
        $db->addColumn('os_name', 'text', ['notnull' => true]);
        $db->addColumn('browser_name', 'text', ['notnull' => true]);
        $db->addColumn('unique_visit', 'integer', ['notnull' => true]);
        $db->addColumn('created_at', 'integer', ['notnull' => true]);
        $db->addIndex(['ip'],'ip_index');
        $db->addIndex(['os_name'],'os_name_index');
        $db->addIndex(['browser_name'],'browser_name_index');
        $db->setPrimaryKey(['id']);

    }

    public function down(Schema $schema): void
    {
        //удаляем таблицу 'logs'
        $schema->dropTable('logs');
        $schema->dropTable('users');

    }
}
