<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\Logs;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;
use Faker\Factory;
use Doctrine\ORM\EntityRepository;

class IndexController extends AbstractActionController
{
    /*
     * Entity manager
     * @var Doctrine\ORM\EntityManager
     */
    private $_em;

    /*
     * Logs manager
     * @var
     */
    private $_logs;

    /*
     * constructor
     */
    public function __construct($em)
    {
        $this->_em = $em;
//        $this->_logs = $logs;
    }

    public function indexAction()
    {


//        $log = $this->_em->getRepository(Logs::class);


        return new ViewModel();
    }

    public function randomAction()
    {
        //Если это запрос на создание рандомных данных
        $request = $this->getRequest()->getQuery();
        if (array_key_exists('rand', $request)) {
            //Мы ждём только integer
            $request['rand'] = (int)$request['rand'];

            //Само собой должно быть больше 0
            if ($request['rand'] > 0) {
                //Создаём массивы для фейковых данных
                $osBrowserList = [
                    'Windows' => 'Edge',
                    'Ubuntu' => 'Chrome',
                    'Linux' => 'Firefox',
                    'Mac' => 'Safari',
                ];

                //Создаём фейкер, для генерации фейковых данных
                $faker = Factory::create();

//                $em = $this->_em->getEventManager('doctrine.entitymanager.orm_default');

                //Time now - day in second
                $startTime = time() - 86400;

                //Заполняем таблицу данными
                for ($i = 0; $i < $request['rand']; $i++) {
                    $randomUniqueVisit = mt_rand(3, 10);

                    $urlFrom = $faker->domainName;
                    $ip = $faker->ipv4;

                    for ($j = 0; $j < $randomUniqueVisit; $j++) {
                        //Получаем рандомное имя ос
                        $os = array_rand($osBrowserList);

                        $log = new Logs();
                        $log->setIp($ip);
                        $log->setUrlFrom($urlFrom);

                        $urlFrom = 'mydomain.com/' . $faker->domainWord;

                        $log->setUrlTo($urlFrom);
                        $log->setOsName($os);
                        $log->setBrowserName($osBrowserList[$os]);
                        $log->setUniqueVisit($j);
                        $log->setCreatedAt($startTime + mt_rand(60, 180));

                        $this->_em->persist($log);
                    }


                }

                $this->_em->flush();

                $em = null;
                $log = null;
                $faker = null;
                $osBrowserList = null;


                //Отдаём ответ, что всё ок
                return new JsonModel([
                    'status' => 'ok',
                ]);
            }
        }

        //Возвращаем форму генерации рандом данных
        return new ViewModel();
    }

    public function dataAction()
    {
        //Получаем все гет запросы
        $request = $this->getRequest()->getQuery();

        //кол-во данных на 1 странице
        $limit = 20;
        //отступ
        $offset = 0;

        //Был ли запрос с отступом
        if (array_key_exists('start', $request) && $request['start'] !== '0') {
            $request['start'] = (int)$request['start'];

            //если пытаются как либо менять запрос вручную, мы это баним
            if ($request['start'] < 0 || $request['start'] % 10 !== 0) {
                return new JsonModel([]);
            }

            $offset = $request['start'];
        }


        //Разрешённые поля для сортировки
        $whiteSort = ['browser_name' => 'browserName', 'os_name' => 'osName'];

        //Сортировка
        $orderBy = [];

        if (array_key_exists('sort', $request)) {
            $request['sort'] = json_decode($request['sort'], true);

            if (
                is_array($request['sort']) &&
                array_key_exists(0, $request['sort']) &&
                array_key_exists('property', $request['sort'][0]) &&
                array_key_exists('direction', $request['sort'][0]) &&
                \array_key_exists($request['sort'][0]['property'], $whiteSort)
            ) {
                $orderBy = [
                    $whiteSort[$request['sort'][0]['property']] => $request['sort'][0]['direction'] === 'ASC' ? 'ASC' : 'DESC',
                ];
            }
        }

        //Поиск по ип
        $filter = [];

        if (array_key_exists('filter', $request)) {
            $request['filter'] = json_decode($request['filter'], true);

            if (
                is_array($request['filter']) &&
                array_key_exists(0, $request['filter']) &&
                array_key_exists('operator', $request['filter'][0]) &&
                array_key_exists('value', $request['filter'][0]) &&
                array_key_exists('property', $request['filter'][0]) &&
                $request['filter'][0]['property'] === 'ip' &&
                $request['filter'][0]['value'] !== ''
            ) {
                //мы знаем, что нужна только фильтрация по ип
                $filter = [
                    'ip' => trim($request['filter'][0]['value']),
                ];
            }
        }

        //Получаем сколько всего записей в базе
        $countEntities = $this->_em->getRepository(Logs::class)->findby([]);
        $count = count($countEntities);
        $countEntities = null;

        //Получаем данные с фильтрацией и сортировкой
        $logEntities = $this->_em->getRepository(Logs::class)
            ->findBy($filter, $orderBy, $limit, $offset);


        $data = [
            'success' => true,
            'total' => $count,
            'rows' => $logEntities,
        ];

        return new JsonModel($data);
    }
}
