<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Logs
 *
 * @ORM\Table(name="logs", indexes={@ORM\Index(name="os_name_index", columns={"os_name"}), @ORM\Index(name="browser_name_index", columns={"browser_name"})})
 * @ORM\Entity
 */
class Logs implements \JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="logs_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", nullable=false)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(name="url_from", type="text", nullable=false)
     */
    private $urlFrom;

    /**
     * @var string
     *
     * @ORM\Column(name="url_to", type="text", nullable=false)
     */
    private $urlTo;

    /**
     * @var string
     *
     * @ORM\Column(name="os_name", type="text", nullable=false)
     */
    private $osName;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_name", type="text", nullable=false)
     */
    private $browserName;

    /**
     * @var int
     *
     * @ORM\Column(name="unique_visit", type="integer", nullable=false)
     */
    private $uniqueVisit;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     */
    private $createdAt;



    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip.
     *
     * @param string $ip
     *
     * @return Logs
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip.
     *
     * @return int
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set urlFrom.
     *
     * @param string $urlFrom
     *
     * @return Logs
     */
    public function setUrlFrom($urlFrom)
    {
        $this->urlFrom = $urlFrom;

        return $this;
    }

    /**
     * Get urlFrom.
     *
     * @return string
     */
    public function getUrlFrom()
    {
        return $this->urlFrom;
    }

    /**
     * Set urlTo.
     *
     * @param string $urlTo
     *
     * @return Logs
     */
    public function setUrlTo($urlTo)
    {
        $this->urlTo = $urlTo;

        return $this;
    }

    /**
     * Get urlTo.
     *
     * @return string
     */
    public function getUrlTo()
    {
        return $this->urlTo;
    }

    /**
     * Set osName.
     *
     * @param string $osName
     *
     * @return Logs
     */
    public function setOsName($osName)
    {
        $this->osName = $osName;

        return $this;
    }

    /**
     * Get osName.
     *
     * @return string
     */
    public function getOsName()
    {
        return $this->osName;
    }

    /**
     * Set browserName.
     *
     * @param string $browserName
     *
     * @return Logs
     */
    public function setBrowserName($browserName)
    {
        $this->browserName = $browserName;

        return $this;
    }

    /**
     * Get browserName.
     *
     * @return string
     */
    public function getBrowserName()
    {
        return $this->browserName;
    }

    /**
     * Set uniqueVisit.
     *
     * @param int $createdAt
     *
     * @return Logs
     */
    public function setUniqueVisit($uniqueVisit)
    {
        $this->uniqueVisit = $uniqueVisit;

        return $this;
    }

    /**
     * Get uniqueVisit.
     *
     * @return int
     */
    public function getUniqueVisit()
    {
        return $this->uniqueVisit;
    }

    /**
     * Set createdAt.
     *
     * @param int $createdAt
     *
     * @return Logs
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'ip' => $this->getIp(),
            'url_from' => $this->getUrlFrom(),
            'url_to' => $this->getUrlTo(),
            'os_name' => $this->getOsName(),
            'browser_name' => $this->getBrowserName(),
            'unique_visit' => $this->getUniqueVisit(),
            'created_at' => $this->getCreatedAt(),
        ];
    }
}
