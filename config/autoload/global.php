<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

use Doctrine\DBAL\Driver\PDOPgSql\Driver as PDOPgSql;

return [
    'doctrine' => [
        //Подключение к базе
        'connection' => [
            'orm_default' => [
                'driverClass' => PDOPgSql::class,
                'params' => [
                    'host'     => '127.0.0.1',
                    'port'     => '5432',
                    'user'     => 'postgres',
                    'password' => '123456789',
                    'dbname'   => 'zend',
                ]
            ],
        ],
        // настройка миграций
        'migrations_configuration' => [
            'orm_default' => [
                'directory' => 'data/Migrations',
                'name'      => 'Doctrine Database Migrations',
                'namespace' => 'Migrations',
                'table'     => 'migrations',
            ],
        ],
    ],
];
